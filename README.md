# **Project: Simple File Management Mobile App**

## **Screen**

![](https://gitlab.com/vutran03699/simple-file-management/-/raw/main/screen/ScreenAppFile.gif?ref_type=heads)

![](https://gitlab.com/vutran03699/simple-file-management/-/raw/main/screen/FileMove.gif?ref_type=heads)## **Problem Statement**

This project aims to develop a simple file management mobile application that allows users to efficiently manage files in shared storage. The app should facilitate the following key features:

- Display a comprehensive list of all files stored in the shared storage.
- Enable users to create a parent folder (1-level deep).
- Allow users to move files into the created parent folder.
- Implement a file viewer screen capable of opening various file types such as PDF, DOC, TXT, and IMAGE (PNG, JPG, etc.).

## **Requirements**

### **File List Screen:**

- [x] Display a list of all files in the shared storage.

- [x] Each file entry should include its name and type (e.g., PDF, DOC, TXT, IMAGE).

- [x] Provide a button to create a new parent folder.

### **Create Parent Folder:**

- [x] Implement a screen to create a parent folder.

- [x] Allow users to input the desired folder name.

- [x] After creating the folder, redirect the user to the File List Screen with the new folder displayed.

### **Move Files:**

- [x] Allow users to select one or more files from the File List Screen.

- [x] Provide an option to move the selected files to the created parent folder.

- [x] Update the File List Screen to reflect the changes after moving files.

### **File Viewer Screen:**

- [x] Develop a file viewer screen capable of opening different file types.

- [x] Support viewing PDF, DOC, TXT, and IMAGE files seamlessly.

## **Assignment Deliverables**

- Test the app on both iOS and Android platforms to ensure no crashes occur.
- Include a comprehensive README file with instructions on how to run the app and any additional notes regarding implementation choices.
- Bonus Work:
  - Implement file and folder search functionality.
  - Incorporate file sharing/sending capabilities.