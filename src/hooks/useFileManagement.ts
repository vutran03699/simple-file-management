import { AttachmentPicker } from '@/screens/File/AttachmentPicker';
import { FileProps } from '@/types/File/FileProps';
import { Platform } from 'react-native';
import OpenFile from 'react-native-files-viewer';
import { removeApplicationPrefix } from '@/untils/removeApplicationPrefix';

interface Doc64Props {
	base64: string;
	fileName: string;
	fileType:
		| 'pdf'
		| 'png'
		| 'jpg'
		| 'xls'
		| 'doc'
		| 'ppt'
		| 'xlsx'
		| 'docx'
		| ' pptx';
	cache: boolean;
	/** ios only */
	fileNameOptional?: string;
}

export const useFileManagement = () => {
	const handlePressb64 = (item: FileProps) => {
		const base64String: string = item.uri.split(';base64,').pop() || '';
		const fileName = item.name;
		const fileType = removeApplicationPrefix(item.type)[1];
		const cache = Platform.OS === 'android';

		const fileData: Doc64Props = {
			base64: base64String,
			fileName,
			fileType: fileType as Doc64Props['fileType'],
			cache,
		};

		const openFileCallback = (error: any, url: any) => {
			if (error) {
				console.error(error);
			} else {
				console.log(url);
			}
		};

		console.log(fileData);
		OpenFile.openDocb64(fileData, openFileCallback);
	};

	return { handlePressb64 };
};
