import { FileProps } from '@/types/File/FileProps';
import { onlyImage } from '@/untils/onlyShow';
import { useEffect, useState } from 'react';
import { Alert, Platform } from 'react-native';
import ImageView from 'react-native-image-viewing';
import OpenFile from 'react-native-files-viewer';
import Storage from '@/services/storage';

interface ImageProps {
	item: string;
}

export interface ImageViewerProps {
	isShow: boolean;
	data: ImageProps[];
	indexCurrent: number;
	setIsVisibleImage: (value: boolean) => void;
}

interface Doc64Props {
	base64: string;
	fileName: string;
	fileType:
		| 'pdf'
		| 'png'
		| 'jpg'
		| 'xls'
		| 'doc'
		| 'ppt'
		| 'xlsx'
		| 'docx'
		| ' pptx';
	cache: boolean;
	/** ios only */
	fileNameOptional?: string;
}

function useFile(file?: FileProps[]) {
	const [isShowImage, setIsShowImage] = useState(false);
	const [currentIndex, setCurrentIndex] = useState(0);
	const [dataShow, setDataShow] = useState<FileProps[]>([]);

	useEffect(() => {
		const dataStorage = Storage.getFiles();
		return setDataShow(dataStorage);
	}, []);
	

	const openFile = ({
		dataStorage,
		item,
	}: {
		dataStorage: FileProps[];
		item: FileProps;
	}) => {
		const { type, uri } = item;

		const isImage = ['image/png', 'image/jpeg', 'image/gif'].includes(type);
		const isDocs = ['application/pdf', 'application/msword'].includes(type);
		const isVideo = ['video/mp4'].includes(type);

		if (isImage) {
			setCurrentIndex(onlyImage(dataStorage).indexOf(item));
			return setIsShowImage(true);
		}
		if (isDocs) {
			// eslint-disable-next-line @typescript-eslint/no-use-before-define
			return handlePressb64(item);
		}
		if (isVideo) {
			return handlePressVideo(item);
		}
		return handlePressb64(item);

	};

	
	const handlePressVideo = (item: FileProps) => {
		Alert.alert('Android coming soon');
	};

	const handleDel = (file?: FileProps) => {
		Alert.alert('Delete', 'Are you sure?', [
			{
				text: 'Cancel',
				onPress: () => console.log('Cancel Pressed'),
				style: 'cancel',
			},
			{
				text: 'OK',
				onPress: () => {
					setDataShow(dataShow.filter(item => item.id !== file?.id));
					// Storage.deleteFile(Number(file?.id));
				},
			},
		]);
	};

	return {
		dataShow,
		image: {
			isShow: isShowImage,
			data: dataShow.map(item => ({ item: item.uri })),
			indexCurrent: currentIndex,
			setIsVisibleImage: (value: boolean) => setIsShowImage(value),
		} as unknown as ImageViewerProps,
		openFile,
		handleDel,
	};
}

export default useFile;
