// hook useDocFile.tsx

import { useEffect, useState } from 'react';
import DocumentPicker from 'react-native-document-picker';

export const useDocFile = () => {
	const [docFiles, setDocFiles] = useState([]);

	const getDocFiles = async () => {
		try {
			const result = await DocumentPicker.pick({
				type: [DocumentPicker.types.doc],
			});

			const docFilePath = result.uri;
			const docFileName = result.name;

			setDocFiles(prevFiles => [
				...prevFiles,
				{ name: docFileName, path: docFilePath },
			]);
		} catch (error) {
			console.log(error);
		}
	};

	// useEffect(() => {
	// 	// You can trigger this function on a button press or in your component's lifecycle
	// 	getDocFiles();
	// }, []);

	return {
		docFiles,
		getDocFiles,
	};
};
