import { SafeScreen } from '@/components/template';
import { addFile, saveFile } from '@/redux/files/filesSlice';
import Storage from '@/services/storage';
import useTheme from '@/theme/hooks/useTheme';
import { FileProps } from '@/types/File/FileProps';
import { ApplicationScreenProps } from '@/types/navigation';
import { UID } from '@/untils/uid';
import { useState } from 'react';
import { Alert, Button, Text, TextInput, View } from 'react-native';
import { useDispatch } from 'react-redux';

interface CreateFileProps {
	navigation: ApplicationScreenProps['navigation'];
	route: {
		params: {
			parentId: number;
		};
	};
}

function CreateParentFolder({ navigation, route }: CreateFileProps) {
	const { parentId } = route.params;
	const dispatch = useDispatch();

	const { layout, gutters, borders } = useTheme();
	const [folderName, setFolderName] = useState<string>('');

	const createParentFolder = () => {
		// check folder name
		if (folderName === '') {
			Alert.alert('Please enter folder name');
			return;
		}

		// Create a new folder object
		const createFile: FileProps = {
			id: UID(),
			name: folderName,
			type: 'folder',
			parentId,
			icon: 'folder',
			createTime: new Date().toISOString(),
			lastTimeUpdated: new Date().toISOString(),
			size: 0,
		};

		dispatch(saveFile(createFile));

		navigation.goBack();
	};

	return (
		<SafeScreen>
			<TextInput
				style={[
					gutters.padding_12,
					layout.flex_1,
					borders.w_1,
					borders.gray400,
				]}
				value={folderName}
				onChangeText={setFolderName}
				placeholder="Enter folder name"
				autoFocus
				keyboardType="default"
				autoCapitalize="none"
				autoCorrect={false}
			/>
			<Button title="Create Folder" onPress={createParentFolder} />
			<Button title="Cancel" onPress={() => navigation.goBack()} />
		</SafeScreen>
	);
}

export default CreateParentFolder;
