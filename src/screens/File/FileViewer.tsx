// FileViewer.tsx
import { View, Button } from 'react-native';
import { ApplicationScreenProps } from '@/types/navigation';
import { useEffect, useLayoutEffect, useState } from 'react';
import useTheme from '@/theme/hooks/useTheme';
import { SafeScreen } from '@/components/template';
import { FileProps } from '@/types/File/FileProps';
import ListItemFiles from '@/components/items/ListItemFiles';
import { onlyShow } from '@/untils/onlyShow';
import { useSelector } from 'react-redux';
import {
	addFilesToMove,
	fetchFiles,
	moveFilesToFolder,
	selectFiles,
	selectMoveFiles,
} from '@/redux/files/filesSlice';
import { store } from '@/redux/store';
import {
	FloatingActionButtonLeft,
	FloatingActionButtonRight,
} from '@/components/button/FloatingActionButton';

interface FileListProps {
	// file: FileProps;
	route: { params: { type: 'all' | 'image' | 'doc' | 'folder' | 'video' } };
	navigation: ApplicationScreenProps['navigation'];
}

function FileViewer({ navigation, route }: FileListProps) {
	const { type = 'all' } = route.params;
	// const isFocused = useIsFocused();

	const { layout, gutters } = useTheme();
	const dataStorage: FileProps[] = useSelector(selectFiles);

	const [selectFilesMove, setSelectFilesMove] = useState<FileProps[]>([]);

	const dataMoveFiles: FileProps[] = useSelector(selectMoveFiles);

	const [filesStorage, setFilesStorage] = useState<FileProps[]>([]);

	const [parentId, setParentId] = useState<string>('');

	useEffect(() => {
		const dataFiter = onlyShow(dataStorage, type as string, true);

		setFilesStorage(dataFiter.filter(item => item.parentId === parentId));
	}, [dataStorage, parentId]);

	useLayoutEffect(() => {
		store.dispatch(fetchFiles());
	}, []);

	const gobackFile = () => {
		const idParent = dataStorage.find(item => item.id === parentId)?.parentId;
		if (parentId === '') {
			return navigation.goBack();
		}
		const data = dataStorage.filter(item => item.parentId === idParent);
		setParentId(idParent as string);
		return setFilesStorage(data);
	};

	const handleFileMove = () => {
		store.dispatch(addFilesToMove(selectFilesMove));
		setSelectFilesMove([]);
	};

	const handleMoveIn = (_parentId: string) => {
		store.dispatch(moveFilesToFolder(_parentId));
	};
	return (
		<SafeScreen>
			<View style={[layout.flex_1, gutters.marginHorizontal_12]}>
				<ListItemFiles
					selectFilesMove={selectFilesMove}
					setSelectFilesMove={setSelectFilesMove}
					filesStorage={filesStorage}
					parentId={parentId}
					setParentId={setParentId}
					setFilesStorage={setFilesStorage}
					type={type}
					sortTimeNew={false}
				/>
				<FloatingActionButtonRight icon="arrow-back-ios" onPress={gobackFile} />
				{selectFilesMove.length > 0 ? (
					<FloatingActionButtonLeft
						icon="drive-file-move"
						onPress={handleFileMove}
					/>
				) : null}
				{dataMoveFiles.length > 0 ? (
					<FloatingActionButtonLeft
						icon="control-point"
						onPress={() => handleMoveIn(parentId)}
					/>
				) : null}
			</View>
		</SafeScreen>
	);
}

export default FileViewer;
