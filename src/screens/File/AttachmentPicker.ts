import { Platform, ActionSheetIOS, Alert } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import { FileProps } from '@/types/File/FileProps';

type Captions = {
	uploadFile: string;
	cancel: string;
	title: string;
};

interface FileGet {
	fileCopyUri: string;
	name: string;
	uri: string;
	type: string;
	size: number;
}

export class AttachmentPicker {
	private captions: Captions;

	constructor(
		captions: Captions = {
			uploadFile: 'Upload File',
			cancel: 'Cancel',
			title: 'Pick type of media',
		},
	) {
		this.captions = captions;
	}

	// Hàm để đọc nội dung của tệp và thực hiện xử lý
	processFile = async (file: FileGet): FileProps => {
		try {
			const { name, size, type, uri } = file;

			const realURI = Platform.select({
				android: uri,
				ios: decodeURI(uri),
			});

			const content = await RNFS.readFile(realURI, 'base64');

			const b64 = `data:${type};base64,${content}`;

			return {
				name,
				size,
				type,
				uri: b64,
				icon: b64,
				lastTimeUpdated: new Date().toISOString(),
				createTime: new Date().toISOString(),
			};
		} catch (error) {
			Alert.alert(`Lỗi khi đọc file ${file.name}:`);
			console.error(`Lỗi khi đọc file ${file.name}:`, error);
			throw error;
		}
	};

	pick = () => {
		if (Platform.OS === 'ios') {
			return this.pickIOS();
		}

		return new Promise((resolve, reject) => {
			return this.pickUpAll(resolve, reject);
		});
	};

	pickIOS = () => {
		return new Promise((resolve, reject) => {
			const { uploadFile, cancel, title } = this.captions;
			const options = [uploadFile, cancel];
			const handlers = [
				this.pickUpAll,
				this.pickClosed,
			];
			const cancelButtonIndex = options.indexOf(cancel);

			ActionSheetIOS.showActionSheetWithOptions(
				{ options, cancelButtonIndex, title },
				buttonIndex => {
					handlers[buttonIndex](resolve, reject);
				},
			);
		});
	};


	pickDocument = async (
		resolve: (payload: any) => void,
		reject: (payload: any) => void,
	) => {
		try {
			const result = await DocumentPicker.pick({
				type: DocumentPicker.types.pdf,
				presentationStyle: 'fullScreen',
				allowMultiSelection: true,
			});

			console.log('result:::', result);

			const fileType = result[0].type;
			const fileExtension = fileType.substr(fileType.indexOf('/') + 1);
			console.log('fileExtension:::', fileExtension);
			const realURI = Platform.select({
				android: result.uri,
				ios: decodeURI(result.uri),
			});
			console.log('realURI:::', realURI);
			const b64Content = await RNFS.readFile(realURI, 'base64');
			console.log('b64Content:::', b64Content);
			const b64 = `data:${fileType};base64,${b64Content}`;
			console.log('b64:::', b64);

			resolve({ b64, fileType, fileExtension });
		} catch {
			reject(new Error('Action cancelled!'));
		}
	};

	pickUpAll = async (
		resolve: (payload: any) => void,
		reject: (payload: any) => void,
	) => {
		try {
			const data = [];
			const result = await DocumentPicker.pick({
				presentationStyle: 'fullScreen',
				allowMultiSelection: true,
			});

			// // vòng lặp
			for (const file of result) {
				const dataProcess = await this.processFile(file);

				data.push(dataProcess);
			}
			resolve(data);

			// resolve({ b64, fileType, fileExtension });
		} catch {
			reject(new Error('Action cancelled!'));
		}
	};

	pickClosed = (_: any, reject: (payload: any) => void) => {
		reject(new Error('Action cancelled! pickUpAll'));
	};
}
