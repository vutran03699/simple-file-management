import { ImageVariant } from '@/components/atoms';
import ListItemFiles from '@/components/items/ListItemFiles';
import SafeScreen from '@/components/template/SafeScreen/SafeScreen';
import Storage from '@/services/storage';
import useTheme from '@/theme/hooks/useTheme';
import { FileProps } from '@/types/File/FileProps';
import { ApplicationScreenProps } from '@/types/navigation';
import { Button, Text, TouchableOpacity, View } from 'react-native';

// ALL,FILES,DOCS, PHOTOS
const Data = [
	{
		name: 'All',
		id: 0,
		icon: 'https://img.icons8.com/fluency-systems-regular/50/f3f6fe/border-all--v2.png',
		color: '#f3727a',
		screen: 'FileViewer',
		type: 'all',
	},
	{
		name: 'Video',
		id: 1,
		icon: 'https://img.icons8.com/ios-filled/50/f3f6fe/opened-folder.png',
		color: '#3ed7b3',
		screen: 'FileViewer',
		type: 'video',
	},
	{
		name: 'Docs',
		id: 2,
		icon: 'https://img.icons8.com/ios-filled/100/f3f6fe/doc.png',
		color: '#56b6fe',
		screen: 'FileViewer',
		type: 'doc',
	},
	{
		name: 'Photos',
		id: 3,
		icon: 'https://img.icons8.com/material-outlined/24/f3f6fe/stack-of-photos--v1.png',
		color: '#eca7fc',
		screen: 'FileViewer',
		type: 'image',
	},
];

// const dataRecentFiles = [
// 	{
// 		id: 0,
// 		name: 'Photos',
// 		describe: '5 Items',
// 		format: 'JPG',
// 		type: 'Photos',
// 		size: '23MB',
// 		icon: 'https://img.icons8.com/ios-filled/100/f3f6fe/stack-of-photos--v1.png',
// 		lastTime: '2024-02-02T15:45:00',
// 		updatedTime: '2024-02-02T15:45:00',
// 	},
// 	{
// 		id: 1,
// 		name: 'Photos',
// 		describe: '5 Items',
// 		format: 'JPG',
// 		type: 'Photos',
// 		size: '23MB',
// 		icon: 'https://img.icons8.com/ios-filled/100/f3f6fe/stack-of-photos--v1.png',
// 		lastTime: '2024-02-02T15:45:00',
// 		updatedTime: '2024-02-02T15:45:00',
// 	},
// 	{
// 		id: 2,
// 		name: 'Docs',
// 		describe: '5 Items',
// 		format: 'xls',
// 		type: 'Docs',
// 		size: '23MB',
// 		icon: 'https://img.icons8.com/ios-filled/100/f3f6fe/stack-of-photos--v1.png',
// 		lastTime: '2024-02-02T15:45:00',
// 		updatedTime: '2024-02-02T15:45:00',
// 	},
// 	{
// 		id: 3,
// 		name: 'Photos',
// 		describe: '5 Items',
// 		format: 'JPG',
// 		type: 'Photos',
// 		size: '23MB',
// 		icon: 'https://img.icons8.com/ios-filled/100/f3f6fe/stack-of-photos--v1.png',
// 		lastTime: '2024-02-02T15:45:00',
// 		updatedTime: '2024-02-02T15:45:00',
// 	},
// ];

function Home({ navigation }: ApplicationScreenProps) {
	const {
		colors,
		variant,
		changeTheme,
		components,
		layout,
		gutters,
		fonts,
		backgrounds,
		borders,
	} = useTheme();
	// const dataStorage = Storage.getFiles();
	return (
		<SafeScreen>
			<View style={[ layout.justifyCenter]}>
				<View style={[gutters.margin_16]}>
					<Text style={[fonts.size_32, fonts.gray200]}>Hi User</Text>
					<Text style={[fonts.bold, fonts.size_32, fonts.gray800]}>
						Welcome Back
					</Text>
				</View>

				{/* <View
					style={[
						backgrounds.purple500,
						borders.rounded_10,
						gutters.marginHorizontal_12,
						gutters.padding_12,
						layout.row,
					]}
				>
					<View style={[layout.flex_1, gutters.padding_24]}>
						<Text style={[fonts.bold, fonts.size_32, gutters.marginBottom_12]}>
							Storage
						</Text>
						<View
							style={[
								layout.row,
								layout.justifyBetween,
								gutters.marginBottom_12,
							]}
						>
							<View style={[layout.flex_1]}>
								<Text style={{}}>Total</Text>
								<Text style={[fonts.bold, fonts.size_24]}>64GB</Text>
							</View>
							<View style={[layout.flex_1]}>
								<Text>Available</Text>
								<Text style={[fonts.bold, fonts.size_24]}>8GB</Text>
							</View>
						</View>
					</View>
				</View> */}

				{/* item here */}
				<View style={[gutters.margin_12]}>
					{Data.map(item => {
						return (
							<TouchableOpacity
								onPress={() =>
									navigation.navigate(item.screen, {
										type: item.type,
									})
								}
								key={item.id}
								style={[
									layout.itemsCenter,
									layout.justifyCenter,
									backgrounds.gray100,
									gutters.margin_12,
									gutters.padding_12,
									borders.rounded_10,
									{
										backgroundColor: item.color,
									},
								]}
							>
								<ImageVariant
									source={{ uri: item.icon }}
									style={[
										// eslint-disable-next-line react-native/no-inline-styles
										{
											width: 50,
											height: 50,
										},
									]}
								/>
								<Text>{item.name}</Text>
							</TouchableOpacity>
						);
					})}
				</View>

				{/* <View style={[layout.flex_2, gutters.margin_12]}>
					<View style={[layout.row, layout.justifyBetween]}>
						<Text style={[fonts.size_24]}>Recent Files</Text>
						<TouchableOpacity>
							<Text style={[fonts.size_16, fonts.gray200]}>View All</Text>
						</TouchableOpacity>
					</View>
					<ListItemFiles type="all" isFolder={false} sortTimeNew />
				</View> */}
			</View>
		</SafeScreen>
	);
}

export default Home;
