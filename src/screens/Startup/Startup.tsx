import { useEffect, useState } from 'react';
import { ActivityIndicator, Text, View } from 'react-native';
import { useQuery } from '@tanstack/react-query';
import { useTranslation } from 'react-i18next';

import { useTheme } from '@/theme';
import { Brand } from '@/components/molecules';
import { SafeScreen } from '@/components/template';

import type { ApplicationScreenProps } from '@/types/navigation';

function Startup({ navigation }: ApplicationScreenProps) {
	const { layout, gutters } = useTheme();
	const [isFetching, setIsFetching] = useState(false);

	useEffect(() => {
		navigation.reset({
			index: 0,
			routes: [{ name: 'Home' }],
		});

		// SIMPLE FETCH
		setTimeout(() => {
			setIsFetching(true);
		}, 1000);
	}, []);

	return (
		<SafeScreen>
			<View
				style={[
					layout.flex_1,
					layout.col,
					layout.itemsCenter,
					layout.justifyCenter,
				]}
			>
				<Brand />
				{isFetching && (
					<ActivityIndicator size="large" style={[gutters.marginVertical_24]} />
				)}
			</View>
		</SafeScreen>
	);
}

export default Startup;
