/* eslint-disable import/no-extraneous-dependencies */
import 'react-native-gesture-handler';
import { MMKV } from 'react-native-mmkv';
import { Provider } from 'react-redux';

import { ThemeProvider } from '@/theme';

import ApplicationNavigator from './navigators/Application';
import './translations';
import { store } from './redux/store';

// const queryClient = new QueryClient();

export const storage = new MMKV();

function App() {
	return (
		<Provider store={store}>
			<ThemeProvider storage={storage}>
				<ApplicationNavigator />
			</ThemeProvider>
		</Provider>
	);
}

export default App;
