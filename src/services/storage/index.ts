import { FileProps } from '@/types/File/FileProps';
import { MMKV } from 'react-native-mmkv';
import { initializeMMKVFlipper } from 'react-native-mmkv-flipper-plugin';

class Storage {
	private static ID = 'test-storage';

	private static SAVE_FILE_KEY_ID = `${this.ID}:SAVE_FILE`;

	private static getStorage() {
		const storage = new MMKV({ id: this.ID });

		if (__DEV__) {
			initializeMMKVFlipper({ default: storage });
		}
		return storage;
	}

	public static getFiles = (): FileProps[] => {
		// this.getStorage().set(
		// 	this.SAVE_FILE_KEY_ID,
		// 	JSON.stringify([
		// 		{
		// 			id: 1,
		// 			icon: 'folder',
		// 			name: 'New Folder',
		// 			type: 'folder',
		// 			lastTimeUpdated: '2023-05-01T12:00:00',
		// 			size: '0',
		// 			createTime: '2023-05-01T12:00:00',
		// 		},
		// 		{
		// 			id: 2,
		// 			icon: 'folder',
		// 			name: 'New Folder2',
		// 			type: 'folder',
		// 			lastTimeUpdated: '2023-05-01T12:00:00',
		// 			size: '0',
		// 			createTime: '2023-05-01T12:00:00',
		// 		},
		// 		{
		// 			id: 3,
		// 			icon: 'Docs',
		// 			name: 'SÁCH KINH TẾ',
		// 			type: 'docs',
		// 			lastTimeUpdated: '2023-05-01T12:00:00',
		// 			size: '339 KB',
		// 			createTime: '2023-05-01T12:00:00',
		// 		},
		// 	]),
		// );

		const savedData = this.getStorage().getString(this.SAVE_FILE_KEY_ID);
		if (savedData) {
			return JSON.parse(savedData) as FileProps[];
		}
		return [];
	};

	/**
	 * createFile
	 */
	public static createFolder({
		name,
		parentId = null,
		type = 'folder',
	}: FileProps) {
		// get all files in storage
		const files = this.getFiles();

		const time = new Date().toISOString();
		// create new file
		const newFile: FileProps = {
			id: files.length + 1,
			parentId,
			icon: 'folder',
			name,
			type,
			lastTimeUpdated: time,
			size: 0,
			createTime: time,
			uri: '',
		};
		files.push(newFile);

		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify(files));

		return newFile;
	}

	public static createFile({item, parentId = null, type }: {
		item: FileProps;
		parentId?: number | null;
		type: string;
	}) {
		// get all files in storage
		const files = this.getFiles();

		const time = new Date().toISOString();
		// create new file
		const newFile = {
			...item,
			id: files.length + 1,
			parentId,
			icon: 'folder',
			lastTimeUpdated: time,
			createTime: time,
		};

		files.push(newFile as FileProps);

		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify(files));

		return newFile;
	}

	public static deleteFile(id: number) {
		const files = this.getFiles();
		const newFiles = files.filter(file => file.id !== id);
		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify(newFiles));
		return newFiles;
	}

	public static renameFile(id: number, name: string) {
		const files = this.getFiles();
		const newFiles = files.map(file => {
			if (file.id === id) {
				return { ...file, name };
			}
			return file;
		});
		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify(newFiles));
	}

	public static moveFile(id: number, parentId: number) {
		const files = this.getFiles();
		const newFiles = files.map(file => {
			if (file.id === id) {
				return { ...file, parentId };
			}
			return file;
		});
		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify(newFiles));
	}

	public static clearStorage() {
		this.getStorage().set(this.SAVE_FILE_KEY_ID, JSON.stringify([]));
	}
}

export default Storage;
