import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

import { Startup } from '@/screens';
import { useTheme } from '@/theme';

import type { ApplicationStackParamList } from '@/types/navigation';
import Home from '@/screens/Home/Home';
import FileViewer from '@/screens/File/FileViewer';
import CreateParentFolder from '@/screens/File/CreateFile';
import PDFList from '@/screens/File/PDFList';

const Stack = createStackNavigator<ApplicationStackParamList>();

function ApplicationNavigator() {
	const { variant, navigationTheme } = useTheme();

	return (
		<NavigationContainer theme={navigationTheme}>
			<Stack.Navigator key={variant} screenOptions={{ headerShown: false }}>
				<Stack.Screen name="Startup" component={Startup} />
				<Stack.Screen name="Home" component={Home} />
				<Stack.Screen name="FileViewer" component={FileViewer} />
				<Stack.Screen
					name="CreateParentFolder"
					component={CreateParentFolder}
				/>
				<Stack.Screen name="PDFList" component={PDFList} />
			</Stack.Navigator>
		</NavigationContainer>
	);
}

export default ApplicationNavigator;
