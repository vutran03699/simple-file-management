export function removeApplicationPrefix(str: string): string[] {
	if (str === 'folder') return ['folder'];
	const parts = str.split('/');
	return parts;
}
