import { FileProps } from '@/types/File/FileProps';
import { removeApplicationPrefix } from './removeApplicationPrefix';

export const onlyAll = (item: FileProps[], isFolder = false) => {
	// only show type all
	if (isFolder) {
		return item;
	}
	return item.filter(i => i.type !== 'folder');
};

export const onlyImage = (item: FileProps[], isFolder = false): FileProps[] => {
	// only show type image
	if (isFolder) {
		return item.filter(
			i =>
				removeApplicationPrefix(i.type)[0] === 'image' || i.type === 'folder',
		);
	}
	return item.filter(i => removeApplicationPrefix(i.type)[0] === 'image');
};
export const onlyDoc = (item: FileProps[], isFolder = false) => {
	// only show type pdf
	if (isFolder) {
		return item.filter(
			i =>
				removeApplicationPrefix(i.type)[0] === 'application' ||
				i.type === 'folder',
		);
	}
	return item.filter(i => removeApplicationPrefix(i.type)[0] === 'application');
};

export const onlyFolder = (item: FileProps[], isFolder = false) => {
	if (isFolder) {
		return item.filter(i => i.type === 'folder');
	}
	// only show type folder
	return item.filter(i => i.type === 'folder');
};

export const onlyVideo = (item: FileProps[], isFolder = false) => {
	// only show type video
	if (isFolder) {
		return item.filter(
			i =>
				removeApplicationPrefix(i.type)[0] === 'video' || i.type === 'folder',
		);
	}
	return item.filter(i => removeApplicationPrefix(i.type)[0] === 'video');
};

export const onlyShow = (item: FileProps[], type: string, isFolder = false) => {
	switch (type) {
		case 'image':
			return onlyImage(item, isFolder);
		case 'doc':
			return onlyDoc(item, isFolder);
		case 'folder':
			return onlyFolder(item, isFolder);
		case 'all':
			return onlyAll(item, isFolder);
		case 'video':
			return onlyVideo(item, isFolder);
		default:
			return item;
	}
};
