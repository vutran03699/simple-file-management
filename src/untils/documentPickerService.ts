import { FileProps } from '@/types/File/FileProps';
import { Platform } from 'react-native';
import DocumentPicker, {
	DocumentPickerResponse,
} from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import { store } from '@/redux/store';
import { saveFile } from '@/redux/files/filesSlice';
import { UID } from './uid';

// Hàm để chọn tập tin từ thiết bị
export const pickDocument = async (): Promise<
	DocumentPickerResponse[] | null
> => {
	try {
		const res = await DocumentPicker.pick({
			type: [DocumentPicker.types.allFiles], // Các loại tệp mà người dùng có thể chọn
		});
		return res;
	} catch (err) {
		console.error('Error picking document:', err);
		return null;
	}
};

// Hàm để xử lý tệp đã chọn
export const handlePickedDocument = (
	file: DocumentPickerResponse[] | null,
	parentId: string, // ID của thư mục cha
) => {
	if (file) {
		try {
			file.forEach(selectedFile => {
				const filePatch =
					Platform.OS === 'ios'
						? selectedFile.uri.replace('file://', '')
						: selectedFile.uri;

				void RNFS.readFile(filePatch, 'base64').then(uriResult => {
					const newFile: FileProps = {
						id: UID(),
						name: selectedFile.name as string,
						uri: uriResult,
						type: selectedFile.type as string,
						size: selectedFile.size as number,
						createTime: new Date().toISOString(),
						lastTimeUpdated: new Date().toISOString(),
						parentId,
						icon: '',
					};

					console.log('File created successfully:', newFile.type);
					store.dispatch(saveFile(newFile));
				});
			});
			return file;
		} catch (error) {
			return console.error('Error creating file:', error);
		}
	} else {
		return console.log('No document picked.');
	}
};
