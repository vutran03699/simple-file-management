import { MMKV } from 'react-native-mmkv';

const mmkv = new MMKV({
	id: 'test-storage-5',
});

// Lấy dữ liệu từ lưu trữ cục bộ
function getDataFromLocal(key: string): Promise<string | undefined> {
	return new Promise((resolve, reject) => {
		try {
			const data = mmkv.getString(key);
			console.log('Retrieved data from local storage:', data);
			resolve(data);
		} catch (error) {
			console.error('Error retrieving data from local storage:', error);
			reject(error);
		}
	});
}

// Lưu dữ liệu vào lưu trữ cục bộ
function saveDataToLocal(key: string, value: string): Promise<void> {
	return new Promise((resolve, reject) => {
		try {
			mmkv.set(key, value);
			resolve();
		} catch (error) {
			console.error('Error saving data to local storage:', error);
			reject(error);
		}
	});
}

export { getDataFromLocal, saveDataToLocal };
