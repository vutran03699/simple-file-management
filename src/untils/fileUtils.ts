import { useDispatch, useSelector } from 'react-redux';
import { selectFiles, setFiles } from '@/redux/files/filesSlice';
import { FileProps } from '@/types/File/FileProps';

// Tạo một custom React Hook để sử dụng trong component hoặc hook khác
export const useFileCreation = () => {
	const dispatch = useDispatch();
	const files = useSelector(selectFiles);

	const createFile = ({
		item,
		parentId = null,
		type,
	}: {
		item: FileProps;
		parentId?: number | null;
		type: string;
	}) => {
		const time = new Date().toISOString();
		const newFile: FileProps = {
			...item,
			id: files.length + 1,
			parentId,
			icon: 'folder',
			lastTimeUpdated: time,
			createTime: time,
		};

		dispatch(setFiles([...files, newFile]));
		return newFile;
	};

	return { createFile };
};
