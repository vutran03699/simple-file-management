export interface FileProps {
	id?: string;
	parentId?: string;
	icon: string;
	name: string;
	type: string;
	lastTimeUpdated: string;
	size: number;
	createTime: string;
	uri?: string;
}

export interface FileListProps {
	files: FileProps[];
	moveFiles: (selectedFiles: FileProps[]) => void;
	setFiles: React.Dispatch<React.SetStateAction<FileProps[]>>;
}

export interface FileViewerProps {
	file: FileProps;
}

export interface CreateFileProps {
	id: string;
	parentId: string;
	icon: string;
	name: string;
	type: 'folder' | 'docs' | 'image' | 'video' | 'audio' | 'pdf' | 'other';
}
