import type { StackScreenProps } from '@react-navigation/stack';

export type ApplicationStackParamList = {
	Startup: undefined;
	Home: undefined;
	FileViewer: {
		type: 'all' | 'image' | 'doc' | 'folder';
	};
	CreateParentFolder: {
		parentId: number | null;
	};
	FileList: undefined;
	PDFList: undefined;
	// FileList: {
	// 	files: File[];
	// 	moveFiles: (selectedFiles: File[]) => void;
	// 	setFiles: React.Dispatch<React.SetStateAction<File[]>>;
	// };
};

export type ApplicationScreenProps =
	StackScreenProps<ApplicationStackParamList>;
