import { ViewStyle } from 'react-native';

export default {
	col: {
		flexDirection: 'column',
	},
	colReverse: {
		flexDirection: 'column-reverse',
	},
	wrap: {
		flexWrap: 'wrap',
	},
	row: {
		flexDirection: 'row',
	},
	rowReverse: {
		flexDirection: 'row-reverse',
	},
	itemsCenter: {
		alignItems: 'center',
	},
	itemsStart: {
		alignItems: 'flex-start',
	},
	itemsStretch: {
		alignItems: 'stretch',
	},
	itemsEnd: {
		alignItems: 'flex-end',
	},
	justifyCenter: {
		justifyContent: 'center',
	},
	justifyAround: {
		justifyContent: 'space-around',
	},
	justifyBetween: {
		justifyContent: 'space-between',
	},
	justifyEnd: {
		justifyContent: 'flex-end',
	},
	justifyStart: {
		justifyContent: 'flex-start',
	},
	/* Sizes Layouts */
	flex_1: {
		flex: 1,
	},
	flex_2: {
		flex: 2,
	},
	flex_3: {
		flex: 3,
	},
	flex_4: {
		flex: 4,
	},
	flex_5: {
		flex: 5,
	},
	flex_6: {
		flex: 6,
	},
	flex_7: {
		flex: 7,
	},
	flex_8: {
		flex: 8,
	},
	flex_9: {
		flex: 9,
	},
	flex_10: {
		flex: 10,
	},
	/* Sizes */
	fullWidth: {
		width: '100%',
	},
	fullHeight: {
		height: '100%',
	},
	/* Positions */
	relative: {
		position: 'relative',
	},
	absolute: {
		position: 'absolute',
	},
	top0: {
		top: 0,
	},
	bottom0: {
		bottom: 0,
	},
	bottom10: {
		bottom: 10,
	},
	bottom20: {
		bottom: 20,
	},
	left0: {
		left: 0,
	},
	right0: {
		right: 0,
	},
	right10: {
		right: 10,
	},
	right20: {
		right: 20,
	},
	z1: {
		zIndex: 1,
	},
	z10: {
		zIndex: 10,
	},
} as const satisfies Record<string, ViewStyle>;
