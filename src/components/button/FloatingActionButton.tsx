/* eslint-disable react/require-default-props */
import { useTheme } from '@/theme';
import { TouchableOpacity, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

interface Props {
	title?: string;
	icon?: string;
	color?: string;
	styleContainer?: any;
	styleText?: any;
	onPress?: () => void;
}

export function FloatingActionButtonRight({
	title = '',
	icon,
	onPress,
	styleContainer,
}: Props) {
	const { layout, gutters, fonts, backgrounds, borders } = useTheme();

	return (
		<TouchableOpacity
			style={[
				layout.absolute,
				layout.flex_1,
				layout.left0,
				layout.bottom20,
				borders.rounded_10,
				gutters.padding_12,
				backgrounds.gray200,
				layout.itemsCenter,
				layout.justifyCenter,
				styleContainer,
			]}
			onPress={onPress}
		>
			<Icon name={icon} size={30} color="white" />
			{title && <Text style={[fonts.bold]}>{title}</Text>}
		</TouchableOpacity>
	);
}

export function FloatingActionButtonLeft({
	title = '',
	icon,
	onPress,
	styleContainer,
}: Props) {
	const { layout, gutters, backgrounds, borders, fonts } = useTheme();

	return (
		<TouchableOpacity
			style={[
				layout.absolute,
				layout.flex_1,
				layout.right0,
				layout.bottom20,
				borders.rounded_10,
				gutters.padding_12,
				backgrounds.gray200,
				layout.itemsCenter,
				layout.justifyCenter,
				styleContainer,
			]}
			onPress={onPress}
		>
			{icon && <Icon name={icon} size={30} color="white" />}
			{title && <Text style={[fonts.bold]}>{title}</Text>}
		</TouchableOpacity>
	);
}

export function FloatingActionButtonCenter({
	title = '',
	icon,
	onPress,
	styleContainer,
}: Props) {
	const { layout, gutters, backgrounds, borders, fonts } = useTheme();

	return (
		<TouchableOpacity
			style={[
				layout.absolute,
				layout.flex_1,
				layout.bottom20,
				layout.right10,
				borders.rounded_10,
				gutters.padding_12,
				backgrounds.gray200,
				layout.itemsCenter,
				layout.justifyCenter,
				styleContainer,
			]}
			onPress={onPress}
		>
			{icon && <Icon name={icon} size={30} color="white" />}
			{title && <Text style={[fonts.bold]}>{title}</Text>}
		</TouchableOpacity>
	);
}
