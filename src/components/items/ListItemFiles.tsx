import { FlatList } from 'react-native';
import { FileProps } from '@/types/File/FileProps';
import { useState } from 'react';
import { deleteFile, selectMoveFiles } from '@/redux/files/filesSlice';
import { useFileManagement } from '@/hooks/useFileManagement';
import { useTheme } from '@/theme';
import { store } from '@/redux/store';
import { useSelector } from 'react-redux';
import EmptyComponent from './EmptyComponent';
import ItemFile from './ItemFile';
import CustomFooterComponent from './CustomFooterComponent';

function ListItemFiles({
	selectFilesMove,
	setSelectFilesMove,
	filesStorage,
	setFilesStorage,
	parentId,
	setParentId,
}: {
	selectFilesMove: FileProps[];
	setSelectFilesMove: (value: FileProps[]) => void;
	filesStorage: FileProps[];
	parentId: string;
	setParentId: (value: string) => void;
	setFilesStorage: (value: FileProps[]) => void;
	sortTimeNew?: boolean;
	isFolder?: boolean;
}) {
	const { handlePressb64 } = useFileManagement();
	const dataMoveFiles: FileProps[] = useSelector(selectMoveFiles);

	const { gutters } = useTheme();

	const [isShowCreateFolder, setIsShowCreateFolder] = useState<boolean>(false);
	// const [selectedFiles, setSelectedFiles] = useState<number[]>([]);

	// const { image, openFile } = useFile();

	// const toggleFileSelection = (fileId: number) => {
	// 	setSelectedFiles(prevSelectedFiles =>
	// 		prevSelectedFiles.includes(fileId)
	// 			? prevSelectedFiles.filter(id => id !== fileId)
	// 			: [...prevSelectedFiles, fileId],
	// 	);
	// };
	

	const handleFileViewer = (file?: FileProps) => {
		if (!file) return;
		if (file.type !== 'folder') {
			// eslint-disable-next-line consistent-return
			return handlePressb64(file);
		}

		const id = file?.id;
		const data = filesStorage.filter(item => item.parentId === id); // tìm file trong folder
		setParentId(id as string);
		setFilesStorage(data);
	};

	const DeleteFile = (file: FileProps) => {
		store.dispatch(deleteFile(file.id as string));
	};

	return (
		<FlatList
			keyExtractor={(item, index) => item.toString() + index.toString()}
			data={isShowCreateFolder ? [] : filesStorage}
			ListEmptyComponent={isShowCreateFolder ? null : <EmptyComponent />}
			ListHeaderComponent={
				<CustomFooterComponent
					parentId={parentId}
					onFocus={value => setIsShowCreateFolder(value)}
				/>
			}
			renderItem={({ index, item }) => (
				<ItemFile
					index={index}
					item={item}
					openFileViewer={handleFileViewer}
					handleDeleteFile={DeleteFile}
					dataMoveFiles={dataMoveFiles}
					selectFilesMove={selectFilesMove}
					setSelectFilesMove={setSelectFilesMove}
				/>
			)}
			keyboardShouldPersistTaps="always"
			style={[gutters.marginTop_12]}
		/>
	);
}

ListItemFiles.defaultProps = {
	type: 'image',
	sortTimeNew: false,
	isFolder: true,
};
export default ListItemFiles;
