import { useTheme } from '@/theme';
import { Text, View } from 'react-native';

function ListFooterComponent() {
	const { layout, gutters, fonts, borders } = useTheme();
	return (
		<View style={[layout.flex_1, layout.itemsCenter, layout.justifyCenter]}>
			<Text style={[fonts.size_24, gutters.padding_24, borders.rounded_16]}>
				No data
			</Text>
		</View>
	);
}

export default ListFooterComponent;
