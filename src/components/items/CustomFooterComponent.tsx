import { saveFile } from '@/redux/files/filesSlice';
import { store } from '@/redux/store';
import useTheme from '@/theme/hooks/useTheme';
import { FileProps } from '@/types/File/FileProps';
import {
	handlePickedDocument,
	pickDocument,
} from '@/untils/documentPickerService';
import { UID } from '@/untils/uid';
import { useEffect, useState } from 'react';
import { Keyboard, TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

function CustomFooterComponent({
	parentId,
	onFocus = () => {},
}: {
	parentId: string;
	onFocus: (value: boolean) => void;
}) {
	const { colors, gutters, backgrounds, borders, layout } = useTheme();
	const [nameFolder, setNameFolder] = useState<string>('');
	const [isShowCreateFolder, setIsShowCreateFolder] = useState<boolean>(false);

	const onFocusInput = (value: boolean, name: string) => {
		if (name === 'folder') {
			setIsShowCreateFolder(value);
		} else {
			onFocus(value);
		}
	};

	// Lister Keyboard on Show
	useEffect(() => {
		const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
			onFocusInput(true, 'folder');
		});
		const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
			onFocusInput(false, 'folder');
			Keyboard.dismiss();
		});
		return () => {
			showSubscription.remove();
			hideSubscription.remove();
		};
	}, []);

	const handleCreateFolder = () => {
		Keyboard.dismiss();

		if (nameFolder === '') {
			return;
		}

		// Create a new folder object
		const createFile: FileProps = {
			id: UID(),
			name: nameFolder,
			type: 'folder',
			parentId,
			icon: 'folder',
			createTime: new Date().toISOString(),
			lastTimeUpdated: new Date().toISOString(),
			size: 0,
		};
		setNameFolder('');
		store.dispatch(saveFile(createFile));
	};

	const handleCreateFile = async () => {
		const pickedDocument = await pickDocument(); // Chọn tập tin
		return handlePickedDocument(pickedDocument, parentId); // Xử lý tập tin đã chọn
	};

	return (
		<View style={[layout.flex_1, layout.row, gutters.marginBottom_12]}>
			<View
				style={[
					layout.flex_1,
					layout.itemsCenter,
					layout.justifyCenter,
					backgrounds.purple500,
					borders.rounded_10,
					gutters.padding_12,
				]}
			>
				<TouchableOpacity onPress={handleCreateFolder}>
					<Icon name="addfolder" size={30} color={colors.primary} />
				</TouchableOpacity>
				<TextInput
					placeholder="Enter folder name"
					value={nameFolder}
					onChangeText={setNameFolder}
					autoCapitalize="none"
					returnKeyType="done"
					onSubmitEditing={handleCreateFolder}
					onFocus={() => onFocusInput(true, 'folder')}
					onBlur={() => onFocusInput(false, 'folder')}
					onPressIn={() => onFocusInput(true, 'folder')}
					clearTextOnFocus
				/>
			</View>
			{!isShowCreateFolder && (
				<>
					<View
						style={{
							width: 20,
							height: '100%',
						}}
					/>
					<TouchableOpacity
						style={[
							layout.flex_1,
							layout.itemsCenter,
							layout.justifyCenter,
							backgrounds.purple500,
							borders.rounded_10,
							gutters.padding_12,
						]}
						onPress={handleCreateFile}
					>
						<Icon name="plus" size={30} color={colors.primary} />
					</TouchableOpacity>
				</>
			)}
		</View>
	);
}

export default CustomFooterComponent;
