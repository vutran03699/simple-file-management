import useTheme from '@/theme/hooks/useTheme';
import { FileProps } from '@/types/File/FileProps';
import { Alert, Image, Pressable, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Layout } from 'react-native-reanimated';
import Storage from '@/services/storage';
import useFile from '@/hooks/useFile';
import { ImageVariant } from '../atoms';

interface ItemFileProps {
	item: FileProps;
	index: number;
	openFileViewer: (item: FileProps) => void;
	handleDeleteFile: (item: FileProps) => void;
	dataMoveFiles: FileProps[];
	selectFilesMove: FileProps[];
	setSelectFilesMove: (value: FileProps[]) => void;
	// openDetail: (item: FileProps) => void;
}

function ItemFile({
	index,
	item,
	openFileViewer,
	handleDeleteFile,
	dataMoveFiles,
	selectFilesMove,
	setSelectFilesMove,
}: ItemFileProps) {
	const { layout, gutters, fonts, colors, backgrounds, borders } = useTheme();

	// const isItemMove =  dataMoveFiles.includes();
	const isItemMove = item.id === dataMoveFiles[dataMoveFiles.length - 1]?.id;
	const thumbnailIcon = (type: string) => {
		if (type.includes('image')) {
			return 'image';
		}
		if (type.includes('pdf')) {
			return 'file-pdf';
		}
		if (type.includes('word')) {
			return 'file-word';
		}
		if (type.includes('audio')) {
			return 'file-audio';
		}
		if (type.includes('video')) {
			return 'file-video';
		}
		if (type.includes('folder')) {
			return 'folder';
		}
		return 'alert-box-outline';
	};

	return (
		<View
			key={index}
			style={[
				layout.flex_1,
				layout.row,
				layout.justifyBetween,
				layout.itemsCenter,
				// opacity if dataMoveFiles includes item => 0.5
				dataMoveFiles.includes(item) && { opacity: 0.5 },
			]}
		>
			{selectFilesMove.length > 0 && (
				<TouchableOpacity
					disabled={dataMoveFiles.includes(item)}
					style={[
						layout.flex_1,
						layout.itemsCenter,
						layout.justifyCenter,
						gutters.padding_12,
						borders.rounded_10,
					]}
					onPress={() => {
						if (selectFilesMove.includes(item)) {
							setSelectFilesMove(
								selectFilesMove.filter(file => file.id !== item.id),
							);
						} else {
							setSelectFilesMove([...selectFilesMove, item]);
						}
					}}
				>
					<MaterialCommunityIcons
						name={
							selectFilesMove.includes(item)
								? 'check-circle'
								: 'checkbox-blank-circle-outline'
						}
						size={30}
						colors={colors.gray100}
					/>
				</TouchableOpacity>
			)}

			<Pressable
				disabled={isItemMove}
				style={[
					layout.flex_10,
					layout.justifyCenter,
					gutters.padding_16,
					backgrounds.purple500,
					borders.rounded_10,
					layout.row,
					gutters.marginBottom_12,
				]}
				onPress={() => openFileViewer(item)}
				onLongPress={() => setSelectFilesMove([...selectFilesMove, item])}
			>
				{/* left */}
				<View style={[layout.flex_1, layout.itemsCenter, layout.justifyCenter]}>
					<MaterialCommunityIcons
						name={thumbnailIcon(item.type)}
						size={40}
						colors={colors.gray100}
					/>
				</View>
				{/* center */}
				<View style={[layout.flex_5]}>
					<Text
						style={[
							fonts.bold,
							fonts.size_24,
							fonts.gray100,
							gutters.paddingBottom_12,
						]}
					>
						{item.name}
					</Text>
					<Text>{item.lastTimeUpdated}</Text>
				</View>
				{/* right */}
				<View style={[layout.flex_1, layout.itemsEnd]}>
					<TouchableOpacity
						disabled={isItemMove}
						onPress={() => handleDeleteFile(item)}
					>
						<MaterialCommunityIcons
							name="delete-outline"
							size={20}
							colors={colors.gray100}
						/>
					</TouchableOpacity>
					<Text>{item.parentId}</Text>
				</View>
			</Pressable>
		</View>
	);
}

export default ItemFile;
