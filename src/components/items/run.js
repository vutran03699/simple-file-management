const tienGuiMoiThang = 2000000; // Số tiền gửi mỗi tháng
const laiSuatHangNam = 0.0565; // Lãi suất hàng năm (5.65%)
const soKyHan = 24; // Số kỳ hạn (24 tháng)

let tongLai = 0;

for (let ky = 1; ky <= soKyHan; ky++) {
  const lai = (tienGuiMoiThang * laiSuatHangNam * ky) / 12;
  tongLai += lai;
}

console.log('Tổng lãi nhận được sau 24 tháng:', tongLai);