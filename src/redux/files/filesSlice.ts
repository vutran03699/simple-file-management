/* eslint-disable import/no-cycle */

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { FileProps } from '@/types/File/FileProps';
import { AppThunk, RootState, store } from '../store';
import { getDataFromLocal, saveDataToLocal } from '../../untils/storageService';

interface FilesState {
	files: FileProps[];
	isLoading: boolean;
	error: string | null;
	moveFiles: FileProps[];
}

const initialState: FilesState = {
	files: [],
	isLoading: false,
	error: null,
	moveFiles: [],
};
const filesSlice = createSlice({
	name: 'files',
	initialState,
	reducers: {
		setFiles: (state, action: PayloadAction<FileProps[]>) => {
			return {
				...state,
				files: action.payload,
			};
		},
		addFile: (state, action: PayloadAction<FileProps>) => {
			return {
				...state,
				files: [...state.files, action.payload] as FileProps[],
			};
		},
		addFiles: (state, action: PayloadAction<FileProps[]>) => {
			return {
				...state,
				files: [...state.files, ...action.payload] as FileProps[],
			};
		},
		setLoading: (state, action: PayloadAction<boolean>) => {
			return {
				...state,
				isLoading: action.payload,
			};
		},
		setError: (state, action: PayloadAction<string | null>) => {
			return {
				...state,
				error: action.payload,
			};
		},
		addFilesToMoved: (state, action: PayloadAction<FileProps[]>) => {
			return {
				...state,
				moveFiles: action.payload,
			};
		},
		clearFilesMoved: state => {
			return {
				...state,
				moveFiles: [],
			};
		},
	},
});

export const {
	setFiles,
	addFile,
	addFiles,
	setLoading,
	setError,
	addFilesToMoved,
	clearFilesMoved,
} = filesSlice.actions;

export const selectFiles = (state: RootState) => state.files.files;
export const selectMoveFiles = (state: RootState) => state.files.moveFiles;
export const selectLoading = (state: RootState) => state.files.isLoading;
export const selectError = (state: RootState) => state.files.error;

export const fetchFiles = (): AppThunk => async dispatch => {
	dispatch(setLoading(true));
	try {
		const storedFiles = await getDataFromLocal('files');

		if (storedFiles) {
			dispatch(setFiles(JSON.parse(storedFiles) as []));
		}
		dispatch(setLoading(false));
	} catch (error) {
		dispatch(setError(error.message as string));
	}
};

export const saveFile =
	(file: FileProps): AppThunk =>
	async dispatch => {
		dispatch(setLoading(true));
		try {
			await saveDataToLocal(
				'files',
				JSON.stringify([...selectFiles(store.getState()), file]),
			);

			dispatch(setLoading(false));
		} catch (error) {
			dispatch(setError(error.message as string));
		} finally {
			dispatch(fetchFiles());
		}
	};

export const saveFiles =
	(files: FileProps[]): AppThunk =>
	async dispatch => {
		dispatch(setLoading(true));
		try {
			await saveDataToLocal('files', JSON.stringify(files));
			dispatch(setLoading(false));
		} catch (error) {
			dispatch(setError(error.message as string));
		} finally {
			dispatch(fetchFiles());
		}
	};

export const addFilesToMove =
	(file: FileProps[]): AppThunk =>
	dispatch => {
		dispatch(addFilesToMoved(file));
	};
// move file to folder with parentId
export const moveFilesToFolder =
	(parentId: string): AppThunk =>
	dispatch => {
		// get file moved to folder change parentId and save
		const fileToMove: FileProps[] = selectMoveFiles(store.getState());
		// loop
		const newFilesStorage = selectFiles(store.getState()).map(file => {
			if (fileToMove.includes(file)) {
				return {
					...file,
					parentId,
				};
			}
			return file;
		});
		dispatch(saveFiles(newFilesStorage));
		dispatch(clearFilesMoved());
		dispatch(fetchFiles());
	};

export const deleteFile =
	(id: string): AppThunk =>
	dispatch => {
		// delete file id and id parentId and save
		const newFilesStorage = selectFiles(store.getState()).filter(
			file => file.id !== id && file.parentId !== id,
		);
		dispatch(saveFiles(newFilesStorage));
	};

const filesReducer = filesSlice.reducer;

export default filesReducer;
